# Finding Prototypes of Answers for Improving Answer Sentence Selection 

This repository is created for storing scripts actually doing the replacement of wh-words in TrecQA data by prototypes of answers mentioned in Tam et al.: Finding Prototypes of Answers for Improving Answer Sentence Selection, IJCAI 2017. Transformed data and untransformed data used in our experiment are also included.

## Purposes of scripts written from scratch

* sharTrecQTCt.py
    * split TrecQA data by question types

* sharWikiQT.py
    * replace the question words in TrecQA data by prototypes